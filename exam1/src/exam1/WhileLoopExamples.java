package exam1;

import java.util.Scanner;

public class WhileLoopExamples {
	
	public static void main(String[] args)
	{
		int stoppingPoint = 0;
		
		// print numbers from 1 to  < stoppingPoint
		
		stoppingPoint = getStoppingPoint(); // note use of helper method
		System.out.println("Here are the positive integers less than " + stoppingPoint);
		int count = 1; 
		while (count < stoppingPoint) // what if stoppingPoint == 1?
		{
			System. out.print(count + " ");
			count = count + 1;
		}
		System.out.println();
		
		// print perfect squares from 0 to < stoppingPoint
		
		stoppingPoint = getStoppingPoint();
		System.out.println("Here are the perfect squares less than " + stoppingPoint);
		int numToSquare = 0;
		while (numToSquare * numToSquare < stoppingPoint)
		{
			System.out.print(numToSquare * numToSquare + " ");
			numToSquare += 1;
		}
		System.out.println(); // note we ensure a line break after each segment terminates
		
		// print Fibonacci numbers from 1 to < stoppingPoint
		
		stoppingPoint = getStoppingPoint();
		int previous = 0;
		int current = 1;
		while (current + previous < stoppingPoint)
		{
			//System.out.print((current + previous) + " "); // parentheses are needed.  What would happen without them?
			System.out.print(current + " ");
			int swap = current;
			current = current + previous; // f_n = f_n-2 + f_n-1
			previous = swap;
		}
		System.out.print(current);
		System.out.println();
		
		
	}
	
	public static int getStoppingPoint()   // method must be static so it can be called from main method in same file
	{
		Scanner in = new Scanner(System.in);
		int stoppingPointVal = 0;
		System.out.print("Please enter a positive integer: ");
		stoppingPointVal = in.nextInt();
		System.out.println();            // print a new line to keep things pretty
		return stoppingPointVal;
	}

}