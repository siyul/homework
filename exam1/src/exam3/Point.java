package exam3;

public class Point implements Comparable<Point> {
	private int x;
	private int y;
	private double distance;

	public Point(int givenX, int givenY) {
		x = givenX;
		y = givenY;
		distance = Math.sqrt( x * x + y * y);
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	@Override
	public int compareTo(Point other) {
		if (this.distance > other.distance) {
			return 1;
		}

		else if (this.distance < other.distance) {
			return -1;
		}

		else {
			return 0;
		}
	}
	
	@Override
	public String toString(){
		return "(" + x + "," + y + ")";
	}
	
//	public String toString(Point[] points){
//		String result = "";
//		for(int i = 0 ; i < points.length; i++){
//			result = result + points[i];
//		}
//		return result;
//	}

}
