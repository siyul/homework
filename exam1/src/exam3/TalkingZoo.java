package exam3;

import java.util.ArrayList;

public class TalkingZoo {
	ArrayList<Animal> animals;
	
	public TalkingZoo(){
		animals = new ArrayList<Animal>();
	}
	
	public void addAniaml(Animal a){
		animals.add(a);
	}
	
	public String everybodyTalkAtOnce(){
		StringBuilder allTalk = new StringBuilder();
		for(int i = 0; i < animals.size(); i ++){
			String eachTalk = animals.get(i).talk();
			allTalk.append(eachTalk);
		}
		return allTalk.toString();
	}
}
