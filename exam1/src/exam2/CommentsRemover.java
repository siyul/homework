package exam2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class CommentsRemover {

	public static void main(String args[]) throws FileNotFoundException {
		// open the file
		Scanner sc = new Scanner(System.in);
		String name = sc.next();
		File file = new File(name);
		Scanner scanner = new Scanner(file);

		int extPosition = name.indexOf(".java");
		String outname = name.substring(0, extPosition) + ".out";
		// Write to file
		File outFile = new File(outname);
		PrintWriter out = new PrintWriter(outFile);

		while (scanner.hasNextLine()) {

			// Get the next line
			String line = scanner.nextLine();

			// Remove leading and training white spaces
			line = line.trim();

			// Process the next line
			if (!line.equals("")) {
				if (isComment(line)) {
					line = scanner.nextLine();
				}

				out.println(line);
			}
		}
		// Close the file
		scanner.close();
		out.close();
	}

	private static boolean isComment(String line) {
		Scanner temp = new Scanner(line);
		String leading = temp.next();
		if (leading.equals("//")) {
			return true;
		} else {
			return false;
		}
	}
}
