package exam2;

public class RangeCounter implements Processor {
	double min;
	double max;
	int count;

	public RangeCounter(double givenMin, double givenMax) {
		min = givenMin;
		max = givenMax;
		count = 0;
	}

	public double getResult() {
		return count;
	}

	public void process(double valToProcess) {
		if (valToProcess < max && valToProcess > min) {
			count++;
		}
	}

}
