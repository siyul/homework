package exam2;

import java.util.Scanner;

public class Contact {
	private String name;
	private int[] phoneNumberArray;
	private String phoneNumber;

	public Contact(String givenName, String givenPhoneNumber) {
		name = givenName;
		phoneNumber = givenPhoneNumber;
		phoneNumberArray = readPhoneNumber(phoneNumber);
	}

	public String getName() {
		return name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public int[] getPhoneNumberArray() {
		return phoneNumberArray;
	}

	private int[] readPhoneNumber(String phnum) {
		Scanner scanner = new Scanner(phnum);
		int[] phoneNumber = new int[10];
		for (int i = 0; i < 10; i++) {
			if (scanner.hasNextInt()) {
				phoneNumber[i] = scanner.nextInt();
			}
		}
		return phoneNumber;
	}
}
