package hw1;
/**
* Model of a coin-operated ParkingMeter is constructed so that a specified
* number of minutes is associated with each of three coin types, nickels,
* dimes, or quarters. .
*
* @author Siyu Lin
*/
public class ParkingMeter{
/**
* The remaining coin values, by default in cents
*/
private int coinRemainingValues;
/**
* The remaining coin numbers
*/
private int coinRemainingNumbers;
/**
* The remaining time by default in minutes
*/
private int timeRemaining;
/**
* The max number of coin that can be inserted
*/
private int coinMax;
/**
* The max time by default in minutes
*/
private int timeMax;
/**
* Added time for one nickel inserted, by default in minutes
*/
private int minutesForNickel;
/**
* Added time for one dime inserted, by default in minutes
*/
private int minutesForDime;
/**
* Added time for one quarter inserted, by default in minutes
*/
private int minutesForQuarter;
/**
* Value of nickel coin in cents.
*/
public static final int NICKEL_VALUE = 5;
/**
* Value of dime coin in cents.
*/
public static final int DIME_VALUE = 10;
/**
* Value of quarter coin in cents.
*/
public static final int QUARTER_VALUE = 25;
/**
* Constructors ParkingTime by default is intially expired and contains no
* coins
*
* @param givenMinutesForNickel
*            Added time for one nickel inserted, by default in minutes,
*            alert error if negative
* @param givenMinutesForDime
*            Added time for one dime inserted, by default in minutes, alert
*            error if negative
* @param givenMinutesForQuarter
*            Added time for one quarter inserted, by default in minutes,
*            alert error if negative
* @param givenMaxTime
*            The max time by default in minutes, alert error if negative
* @param givenMaxCoins
*            The max number of coin that can be inserted, alert error if
*            negative
*/
public ParkingMeter(int givenMinutesForNickel, int givenMinutesForDime,
int givenMinutesForQuarter, int givenMaxTime, int givenMaxCoins) {
timeRemaining = 0;
coinMax = givenMaxCoins;
timeMax = givenMaxTime;
minutesForNickel = givenMinutesForNickel;
minutesForDime = givenMinutesForDime;
minutesForQuarter = givenMinutesForQuarter;
if (minutesForNickel > minutesForDime | minutesForDime > minutesForQuarter)
{
}
if (!(minutesForNickel > 0 && minutesForDime > 0 && minutesForQuarter > 0))
{
}
}
/**
* simulateTimeSimulates the passage of time for the given number of
* minutes. Does nothing if the given value is negative.
*
* @param minutes
*            The passage of time, in minutes by default
*/
public void simulateTime(int minutes) {
		int elapsedMinutes = minutes;
if (minutes > 0)
{
if (timeRemaining - minutes < 0)
{
elapsedMinutes = timeRemaining;
}
else
{
elapsedMinutes = minutes;
;
}
}
else
{
}
timeRemaining = timeRemaining - elapsedMinutes;
}
/**
* emptyCoins reduces the coin to zero, without modifying the time
*/
public void emptyCoins() {
coinRemainingNumbers = 0;
coinRemainingValues = 0;
}
/**
* getCents returns the total amount of money, in cents, contained in this
* meter
*
* @return the total amount of mone
*/
public int getCents() {
return coinRemainingValues;
}
/**
* getDollarString returns a String representing the total amount of money,
* in dollars
*
* @return the total amount of money
*/
public String getDollarString() {
String result = String.format("%01.2f", coinRemainingValues * 0.01);
return result;
}
/**
* getMinutesRemaining returns the amount of time, in minutes, remaining on
* this meter
*
* @return the amount of time
*/
public int getMinutesRemaining() {
return timeRemaining;
}
/**
* getHourMinuteString() returns a String representing the time remaining on
* the meter in the form "hh:mm"'
*
* @return the time remaining on the meter in the form "hh:mm"'
*/
public String getHourMinuteString() {
int h = timeRemaining / 60;
int m = timeRemaining - h * 60;
String result = String.format("%02d:%02d", h, m);
return result;
}
/**
* getTotalCoins() returns the number of coins currently in this meter
*
* @return the number of coins
*/
public int getTotalCoins() {
return coinRemainingNumbers;
}
/**
* addingCoins will increase the coinRemainingNumber for a given number of
* coins inserted until the coinMax
*
* @param howMany
*            The number of coins inserted
*/
private void addingCoins(int howMany) {
int addedCoins = howMany;
if (addedCoins < 0)
{
addedCoins = 0;
}
else
{
coinRemainingNumbers = Math.min(coinRemainingNumbers + addedCoins, coinMax);
}
}
/**
* insertDimes(int howMany) inserts a given number of dimes into this meter,
*
* @param howMany
*            The number of coins inserted
*/
public void insertDimes(int howMany) {
//Number of the dimes added
int addedDimesNumber;
addedDimesNumber = howMany;
int currentCoinRemainingNumbers = coinRemainingNumbers;
//Increase the coin numbers after inserting the dimes
this.addingCoins(addedDimesNumber);
//Coins can only be inserted until the coinMax
if (coinRemainingNumbers == coinMax)
{
addedDimesNumber = coinMax - currentCoinRemainingNumbers;
}
//Increase time after inserting coins
//Check the validity of the parameter howMany
if (addedDimesNumber > 0)
{
//It would be igored if the time has exceeded the max time amount
timeRemaining = Math.min(timeRemaining + addedDimesNumber * minutesForDime, timeMax);
//Only when the timeRemaining has exceeded the timeMax and the coinRemainingNumbers has not exceeded the coinMax,
//the coinRemainingValues can be added
if ((timeRemaining <= timeMax) && (coinRemainingNumbers <= coinMax))
{
coinRemainingValues = coinRemainingValues + addedDimesNumber * DIME_VALUE;
}
else
{
coinRemainingValues = coinRemainingValues;
}
}
}
/**
* insertNickels(int howMany) inserts a given number of dimes into this
* meter,
*
* @param howMany
*            The number of coins inserted
*/
public void insertNickels(int howMany) {
//Number of the nickels added
int addedNickelsNumber;
addedNickelsNumber = howMany;
int currentCoinRemainingNumbers = coinRemainingNumbers;
this.addingCoins(addedNickelsNumber);
if (coinRemainingNumbers == coinMax)
{
addedNickelsNumber = coinMax - currentCoinRemainingNumbers;
}
if (addedNickelsNumber > 0)
{
//It would be igored if the time has exceeded the max time amount
timeRemaining = Math.min(timeRemaining + addedNickelsNumber * minutesForNickel, timeMax);
//Only when the timeRemaining has exceeded the timeMax and the coinRemainingNumbers has not exceeded the coinMax,
//the coinRemainingValues can be added
if ((timeRemaining <= timeMax) && (coinRemainingNumbers <= coinMax))
{
coinRemainingValues = coinRemainingValues + addedNickelsNumber * NICKEL_VALUE;
}
else
{
coinRemainingValues = coinRemainingValues;
}
}
}
/**
* insertQuarters(int howMany) inserts a given number of dimes into this
* meter,
*
* @param howMany
*            The number of coins inserted
*/
public void insertQuarters(int howMany) {
int addedQuartersNumber;
addedQuartersNumber = howMany;
int currentCoinRemainingNumbers = coinRemainingNumbers;
this.addingCoins(addedQuartersNumber);
if (coinRemainingNumbers == coinMax)
{
addedQuartersNumber = coinMax - currentCoinRemainingNumbers;
}
if (addedQuartersNumber > 0)
{
///It would be igored if the time has exceeded the max time amount
timeRemaining = Math.min(timeRemaining + addedQuartersNumber * minutesForQuarter,timeMax);
//Only when the timeRemaining has exceeded the timeMax and the coinRemainingNumbers has not exceeded the coinMax,
//the coinRemainingValues can be added
if ((timeRemaining <= timeMax) && (coinRemainingNumbers <= coinMax))
{
coinRemainingValues = coinRemainingValues + addedQuartersNumber * QUARTER_VALUE;
}
else
{
coinRemainingValues = coinRemainingValues;
}
}
}
/**
* isExpired() returns true if there is no time remaining, false otherwise.
*
* @return returns true if there is no time remaining, false otherwise.
*/
public boolean isExpired() {
if (timeRemaining > 0)
{
return false;
}
else
{
return true;
}
}
/**
* toString returns a String representation of this object in exactly the
* following format: hw1.ParkingMeter
* [" + time + "," + number + "," + value + "]
*
* @return a String representation
*/
public String toString() {
String time = "Time" + timeRemaining + "/" + timeMax;
String number = "Coins" + coinRemainingNumbers + "/" + coinMax;
String value = "Value" + " " + coinRemainingValues;
String result = "hw1.ParkingMeter [" + time + "," + number + "," + value + "]";
return result;
}
}
