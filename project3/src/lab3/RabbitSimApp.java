package lab3;

import plotter.SimulationPlotter;;

public class RabbitSimApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Construct a SimulationPlotter 
		SimulationPlotter plotter =  new SimulationPlotter();
		RabbitModel rb = new RabbitModel();
		plotter.simulate(rb);
		
		//RabbitModel2 rb2 = new RabbitModel2();
		//plotter.simulate(rb2);
		
		//RabbitModel3 rb3 = new RabbitModel3();
		//plotter.simulate(rb3);
		
	}

}
