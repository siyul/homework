package lab3;

import java.util.Random;

public class RabbitModel {
	//The population is initially two,one new rabbit is born each year, and no rabbits ever die.
	//Instance Variables
	private int population;
	private int lastYearPopulation;
	
	//Constructors
	public RabbitModel(){
		reset();
	}
	// Methods getPopulation  that returns an int representing the number of rabbits alive at the end of the current year of the simulation
	public int getPopulation () 
	{
		return population;
	}
	
	// method named simulateYear that simulates the passage of a year. 
	// This method updates the number of rabbits alive at the end of the year, and adjusts any additional state (instance variables) of the model.
	// In the simulateYear method, increment this variable by 1 to simulate the passing of one year.
	public void simulateYear()
	{
		
		//1
		//population ++;

		int tempPopulation = population;
		population = population + lastYearPopulation; 
		lastYearPopulation = tempPopulation;

		
		
	}
	
	//  method named reset which initializes any instance variables to their values at the start of the simulation. It takes no arguments.
	// In the reset method, set this variable back to the initial value 2. 
	// (Note that this reset always does the exact same work as the constructor, so in more complex models you can save yourself some effort by just calling reset in your constructor.)
	public void reset()
	{
		lastYearPopulation = 1;
		population = 1 ;
	}
	
}
