package lab7;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class TextEditor {

	private static Scanner in;

	public static void main(String[] args) throws FileNotFoundException {
		in = new Scanner(System.in);
		File outFile = new File("myDocument.txt");
		PrintWriter out = new PrintWriter(outFile);
		
		//Echo keyboard input out to the file
		while (in.hasNextLine()){
			String line = in.nextLine();
			out.println(line);
			System.out.println(line);
		}
		
		out.close();
	}

}
