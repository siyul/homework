package mini1;

public class TVTester {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TV testTV = new TV(3);
		System.out.println(testTV.getChannel()); // Get the Initial Channel
		testTV.channelUp();
		testTV.channelUp();
		System.out.println(testTV.getChannel()); // Get the Channel after adding 1
		System.out.println(testTV.getVolume()); // Get the Initial Volume
		testTV.volumeDown();
		System.out.println(testTV.getVolume());
		testTV.goToPreviousChannel(); // Get the Previous Channel
		System.out.println(testTV.getChannel());
	}

}
