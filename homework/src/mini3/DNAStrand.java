package mini3;

public class DNAStrand {
	// Instance Variables
	private String data = "";
	private int length;

	// Constructors
	public DNAStrand(String givenData) {
		data = data + givenData;
		length = data.length();
	}

	// Methods

	// Returns the number of matching pairs when 'other' is shifted to the left
	// by 'shift' spaces.
	public int countMatchesWithLeftShift(DNAStrand other, int shift) {
		String matchesFragment = findMatchesWithLeftShift(other, shift);
		int count = matchesFragment.length();
		for (int i = 0; i < matchesFragment.length(); i++)
		{
			if (Character.isLowerCase(matchesFragment.charAt(i)))
			{
				count--;
			}
		}
		return count;
	}

	// Returns the number of matching pairs when 'other' is shifted to the right
	// by 'shift' spaces.
	public int countMatchesWithRightShift(DNAStrand other, int shift) {
		String matchesFragment = findMatchesWithRightShift(other, shift);
		int count = matchesFragment.length();
		for (int i = 0; i < matchesFragment.length(); i++)
		{
			if (Character.isLowerCase(matchesFragment.charAt(i)))
			{
				count--;
			}
		}
		return count;
	}

	// Returns a new DNAStrand that is the complement of this one; that is, 'A'
	// is replaced with 'T' and so on.
	public DNAStrand createComplement() {
		String complement = "";
		for (int i = 0; i < length; i++)
		{
			char currentBase = data.charAt(i);
			complement += findBaseComplement(currentBase);
		}
		DNAStrand complementStrand = new DNAStrand(complement);
		return complementStrand;
	}

	private String findBaseComplement(char base) {
		if (base == 'A')
		{
			return "T";
		}
		else if (base == 'T')
		{
			return "A";
		}
		else if (base == 'C')
		{
			return "G";
		}
		else if (base == 'G')
		{
			return "C";
		}
		else
			return "";
	}

	private String findMatchesOfSameLength(String fragment1, String fragment2) {
		String fragment1Match = "";
		// String fragment2Match = "";
		int fragmentLen = fragment1.length();
		if (fragment2.length() != fragmentLen)
		{
			return "";
		}
		for (int i = 0; i < fragmentLen; i++)
		{
			boolean isBaseMatch = matches(fragment1.charAt(i), fragment2.charAt(i));
			if (isBaseMatch)
			{
				fragment1Match += fragment1.substring(i, i + 1);
				// fragment2Match += fragment2.substring(i, i+1);
			}
			else
			{
				fragment1Match += fragment1.substring(i, i + 1).toLowerCase();
			}
		}
		return fragment1Match;
	}

	// Returns a string showing which characters in this strand are matched with
	// 'other' when shifted left by the given amount.
	public String findMatchesWithLeftShift(DNAStrand other, int shift) {
		String otherFragment = "";
		String thisFragment = "";
		if (shift >= other.length())
		{
			return "";
		}
		if (this.length() >= other.length())
		{
			otherFragment = other.data.substring(shift);
			thisFragment = this.data.substring(0, otherFragment.length());
		}
		if (this.length() < other.length())
		{
			if (shift <= (other.length - this.length))
			{
				thisFragment = data;
				otherFragment = other.data.substring(shift, shift + thisFragment.length());
			}
			else
			{
				otherFragment = other.data.substring(shift);
				thisFragment = data.substring(0, otherFragment.length());
			}
		}
		String fragment = findMatchesOfSameLength(thisFragment, otherFragment);
		return fragment + data.substring(fragment.length()).toLowerCase();

	}

	// Returns a string showing which characters in this strand are matched with
	// 'other' when shifted right by the given amount.
	public String findMatchesWithRightShift(DNAStrand other, int shift) {
		// DNAStrand givenStrand = other;
		// return givenStrand.findMatchesWithLeftShift(this, shift);
		String otherFragment = "";
		String thisFragment = "";
		if (shift >= this.length())
		{
			return "";
		}
		if (this.length() >= other.length())
		{
			if (shift >= (this.length() - other.length()))
			{
				thisFragment = data.substring(shift);
				otherFragment = other.data.substring(0, thisFragment.length());
			}
			else
			{
				otherFragment = other.data;
				thisFragment = this.data.substring(shift, shift + otherFragment.length());
			}
		}
		if (this.length() < other.length())
		{
			thisFragment = data.substring(shift);
			otherFragment = other.data.substring(0, thisFragment.length());
		}

		String fragment = findMatchesOfSameLength(thisFragment, otherFragment);
		return this.data.substring(0, shift).toLowerCase() + fragment + this.data.substring(shift + fragment.length()).toLowerCase() ;
	}

	// Returns the maximum possible number of matching base pairs when the given
	// sequence is shifted left or right by any amount.
	public int findMaxPossibleMatches(DNAStrand other) {
		int lenCompare = Math.min(this.length(), other.length());
		int leftShiftMaxMatches = 0;
		int rightShiftMaxMatches = 0;
		for (int i = 0; i < lenCompare; i++)
		{
			leftShiftMaxMatches = Math
					.max(leftShiftMaxMatches, countMatchesWithLeftShift(other, i));
			rightShiftMaxMatches = Math.max(rightShiftMaxMatches,
					countMatchesWithRightShift(other, i));
		}
		int maxPossibleMatches = Math.max(leftShiftMaxMatches, rightShiftMaxMatches);
		return maxPossibleMatches;
	}

	// Determines whether all characters in this strand are valid ('A', 'G',
	// 'C', or 'T').
	public boolean isValid() {
		// int i = 0 ;
		// boolean charValid = false;
		for (int i = 0; i < data.length(); i++)
		{
			char c = data.charAt(i);
			if (!isCharValid(c))
			{
				return false;
			}
		}
		return true;
	}

	private boolean isCharValid(char c) {
		// boolean result = false ;
		if (c == 'A' || c == 'G' || c == 'C' || c == 'T')
		{
			return true;
		}
		else
			return false;
	}

	// Returns the length of this strand.
	public int length() {
		return length;
	}

	// Counts the number of occurrences of the given character in this strand.
	public int letterCount(char ch) {
		int count = 0;
		for (int i = 0; i < length; i++)
		{
			char c = data.charAt(i);
			if (c == ch)
			{
				count++;
			}
		}
		return count;
	}

	// Returns true if the two characters form a base pair ('A' with 'T' or 'C'
	// with 'G').
	public boolean matches(char c1, char c2) {
		if (((c1 == 'A') && (c2 == 'T')) || ((c1 == 'C') && (c2 == 'G'))
				|| ((c1 == 'T') && (c2 == 'A')) || ((c1 == 'G') && (c2 == 'C')))
		{
			return true;
		}
		else
			return false;

	}

	// Returns a String representing the data for this DNAStrand.
	public String toString() {
		return data;
	}

}
