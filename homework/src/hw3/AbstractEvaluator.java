package hw3;

import java.util.ArrayList;
import java.util.Arrays;

import api.Card;
import api.Hand;
import api.IEvaluator;
import api.Suit;

/**
 * The class AbstractEvaluator includes common code for all evaluator types.
 * 
 */
public abstract class AbstractEvaluator implements IEvaluator {
	protected String name;
	protected int ranking;
	protected int handSize;
	protected int numMainCards;

	// Abstract Methods
	public AbstractEvaluator() {
	}

	public abstract boolean canSatisfy(Card[] mainCards);

	public String getName() {
		return name;
	}

	public int getRanking() {
		return ranking;
	}

	public int cardsRequired() {
		return numMainCards;
	}

	public int handSize() {
		return handSize;
	}

	public boolean canSubsetSatisfy(Card[] allCards) {
		if (findMainSubCards(allCards).size() != 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Search given ranks among all cards and return the best hand that contains
	 * cards of given ranks
	 * 
	 * @param allCards
	 *            Given allCards
	 * @param ranks
	 *            Given ranks
	 * @return The best hand containing cards of given ranks, return null of if
	 *         given cards are less than the required cards, or no ranks can
	 *         satisfy the evaluator or the given ranks can not be found in all
	 *         cards
	 */
	protected Hand createHandByRank(Card[] allCards, int[] ranks) {

		// Can we find given ranks in allCards?
		boolean found = findRanks(allCards, ranks);

		// Return Null If
		// The given cards are less than the required cards OR
		// No ranks can satisfy the criteria OR
		// The given ranks can not be found in all cards
		if (allCards.length < this.handSize()
				|| !this.canSubsetSatisfy(allCards) || !found) {
			return null;
		} else {

			Hand hand;
			// Find main cards that contain subset and meet the criteria
			Card[] mainCards = new Card[this.cardsRequired()];
			Card[] sideCards = new Card[this.handSize() - this.cardsRequired()];

			mainCards = findBestMainCards(allCards, ranks);

			// When no given rank is contained in satisfiedSubCard
			if (mainCards == null) {
				return null;
			} else {
				findBestSideCards(allCards, mainCards, sideCards);
			}

			hand = new Hand(mainCards, sideCards, this);
			return hand;
		}
	}

	public Hand createHand(Card[] allCards, int[] subset) {
		Card[] mainCards = new Card[subset.length];
		if (subset.length < cardsRequired() || !this.canSubsetSatisfy(allCards)
				|| allCards.length < this.handSize()) {
			return null;
		} else {
			for (int i = 0; i < mainCards.length; i++) {
				int indices = subset[i];
				mainCards[i] = allCards[indices];
			}
			Card[] sideCards = new Card[this.handSize() - this.cardsRequired()];
			if (canSatisfy(mainCards)) {
				findBestSideCards(allCards, mainCards, sideCards);
			} else {
				return null;
			}
			return new Hand(mainCards, sideCards, this);
		}
	}

	public Hand getBestHand(Card[] allCards) {
		if (allCards.length < handSize) {
			return null;
		} else {
			// Find best mainCards from all possible mainCards and compare
			int[] ranks = new int[allCards.length];
			// Copy all the rankings to subset
			for (int i = 0; i < allCards.length; i++) {
				ranks[i] = allCards[i].getRank();
			}
			Hand hand = createHandByRank(allCards, ranks);
			return hand;
		}
	}

	/**
	 * Search all possible main cards in all cards array list
	 * 
	 * @param allCards
	 * @return An array list of all combinations of main cards that can satisfy
	 *         the evaluator
	 */
	// Find all mainCards that canSatisfy will return true
	private ArrayList<Card[]> findMainSubCards(Card[] allCards) {

		// Create all possible subsets
		ArrayList<Card[]> subCards = createSubsetsList(allCards,
				this.cardsRequired());
		ArrayList<Card[]> mainCardsList = new ArrayList<Card[]>();

		// Find mainCards from all possible subset
		for (int i = 0; i < subCards.size(); i++) {
			if (this.canSatisfy(subCards.get(i))) {
				mainCardsList.add(subCards.get(i));
			}
		}
		return mainCardsList;
	}

	/**
	 * Find all the subsets of given size from all cards
	 * 
	 * @param allCards
	 *            The Card array from which finding subsets
	 * @param size
	 *            The size of the subset
	 * @return An array list of subsets of cards, and each subset's size is
	 *         given size
	 */
	protected ArrayList<Card[]> createSubsetsList(Card[] allCards, int size) {
		// How many subsets whose length is given size are in allCards?
		ArrayList<int[]> subsets = util.SubsetFinder.findSubsets(
				allCards.length, size);
		ArrayList<Card[]> subCards = new ArrayList<Card[]>();

		for (int i = 0; i < subsets.size(); i++) {
			int[] subset = subsets.get(i);

			Card[] subsetCards = new Card[size];
			int resultIndex = 0;
			for (int j = 0; j < subset.length; ++j) {
				int index = subset[j];
				Card card = allCards[index];

				subsetCards[resultIndex] = card;
				resultIndex += 1;
			}

			subCards.add(subsetCards);
		}
		return subCards;
	}

	/**
	 * Finding best side cards by given allCards, mainCards and alter the
	 * initially-null side cards if we have found one. The mainCards can satisfy
	 * the evaluator and the side cards would be the cards that come before any
	 * other cards except the main cards. The sum of length of mainCards and
	 * sideCards will be the handSize of the evaluator.
	 * 
	 * @param allCards
	 *            All the cards that have already been sorted
	 * @param mainCards
	 *            Main cards can satisfy the evaluator
	 * @param sideCards
	 *            Initially null, but if we have found the best side cards, it
	 *            would be changed.
	 */
	private void findBestSideCards(Card[] allCards, Card[] mainCards,
			Card[] sideCards) {
		// Find side cards that come before any other cards according to the
		// handSize

		// An array list to store all possible side cards
		ArrayList<Card> allPossibleSideCards = new ArrayList<Card>();
		cardsListGen(allCards, allPossibleSideCards);

		ArrayList<Card> remainingMainCards = new ArrayList<Card>();
		cardsListGen(mainCards, remainingMainCards);

		// Find and remove mainCards from allCards
		// Initially no card from mainCards is chosen
		Card chosenCard = null;
		findRemove(allPossibleSideCards, remainingMainCards, chosenCard);

		// An array to store the best side cards
		Card[] allSideCardsArray = new Card[allPossibleSideCards.size()];

		// allSideCards to array
		for (int i = 0; i < allPossibleSideCards.size(); i++) {
			allSideCardsArray[i] = allPossibleSideCards.get(i);
		}

		// The best sides cards should be the first sideCards.length cards
		for (int i = 0; i < sideCards.length; i++) {
			sideCards[i] = allSideCardsArray[i];
		}
	}

	/**
	 * Convert an array of cards to an array list of cards
	 * 
	 * @param allCards
	 *            An array of all cards
	 * @param allCardsList
	 *            The resulted array list of all cards
	 */
	protected void cardsListGen(Card[] allCards, ArrayList<Card> allCardsList) {
		for (int i = 0; i < allCards.length; i++) {
			allCardsList.add(allCards[i]);
		}
	}

	/**
	 * Find and remove remaining cards from an array list of all cards and alter
	 * all cards array list
	 * 
	 * @param allCards
	 *            Decreased if we have found and remove remaining cards from it
	 * @param remaining
	 *            The remaining cards to be found and removed
	 * @param chosen
	 *            Keeping track of the cards we have found and remove
	 */
	protected void findRemove(ArrayList<Card> allCards,
			ArrayList<Card> remaining, Card chosen) {

		// Dead End
		if (remaining.size() == 0) {
			return;
		} else {
			chosen = remaining.get(0);
			int chosenCardPosition = find(allCards, chosen);
			if (chosenCardPosition != -1) {
				remaining.remove(0);
				allCards.remove(chosenCardPosition);
				findRemove(allCards, remaining, chosen);
			} else {
				return;
			}
		}
	}

	/**
	 * Find the index of first card that is identical to the given card from
	 * allCards
	 * 
	 * @param allCards
	 *            An array list of all cards that may contain the given card
	 * @param card
	 *            The card to be found
	 * @return The index of first card that is equal to given card return -1 if
	 *         we can not find
	 */
	protected int find(ArrayList<Card> allCards, Card card) {
		int index = -1;
		boolean found = false;
		while (!found && index < (allCards.size() - 1)) {

			if (card.equals(allCards.get(index + 1))) {
				found = true;
				return index + 1;
			}

			index++;
		}

		return index;
	}

	/**
	 * Find given ranks from given all cards
	 * 
	 * @param allCards
	 *            An array of all cards that may contain cards that have given
	 *            ranks
	 * @param ranks
	 *            Given ranks
	 * @return true if given ranks are found in all cards
	 */
	protected boolean findRanks(Card[] allCards, int[] ranks) {

		// Find if the indices of subset appear in the allCards
		// Return null if not found
		for (int e : ranks) {
			boolean found = false;
			int index = 0;
			while (!found && index < allCards.length) {
				if (allCards[index].getRank() == e) {
					found = true;
					return found;
				}
				index++;
			}
		}
		return false;
	}

	/**
	 * Find best main cards that can satisfy the evaluator of given ranks in an
	 * array of all cards
	 * 
	 * @param allCards
	 *            All cards that may contain best main cards of given ranks
	 * @param ranks
	 *            Best main cards must contain at least one of given ranks
	 * @return Best main cards that contain given ranks and satisfy the
	 *         evaluator. Return null if we can not find
	 */
	private Card[] findBestMainCards(Card[] allCards, int[] ranks) {
		Card[] mainCards = new Card[this.cardsRequired()];

		// Iterate all the mainCards subsets
		ArrayList<Card[]> satisfiedSubCard = findMainSubCards(allCards);
		// Assume mainCards comes after other mainCards subsets
		for (int i = 0; i < mainCards.length; i++) {
			mainCards[i] = new Card(2, Suit.CLUBS);
		}

		// Find the best main cards that comes before any other mainCards
		// subsets
		for (Card[] cards : satisfiedSubCard) {

			// When we find given indices in an element of satifiedSubCard
			if (findRanks(cards, ranks)) {
				// mainCards should come before any other mainCards subsets
				if (isBefore(cards, mainCards)) {
					mainCards = Arrays.copyOf(cards, mainCards.length);
				}
			} else {
				return null;
			}
		}
		return mainCards;
	}

	/**
	 * Compare two card arrays and return true if the first given card array
	 * comes before the second given card array
	 * 
	 * @param lhs
	 *            The first given card array
	 * @param rhs
	 *            The second given card array
	 * @return True if the first given array comes before the second one, and
	 *         false otherwise
	 */
	private boolean isBefore(Card[] lhs, Card[] rhs) {
		Hand lhsHand = new Hand(lhs, null, new CatchAllEvaluator(this.ranking,
				lhs.length));
		Hand rhsHand = new Hand(rhs, null, new CatchAllEvaluator(this.ranking,
				lhs.length));
		return lhsHand.compareTo(rhsHand) < 0;
	}

	/**
	 * Check the given cards are same kind
	 * 
	 * @param cards
	 *            An array list of given cards
	 * @return return false if any two of them is not the same kind
	 */
	protected boolean allSameKind(ArrayList<Card> cards) {
		for (int i = 0; i < cards.size() - 1; i++) {
			boolean twoCardsSameRank = (cards.get(i).compareToIgnoreSuit(
					cards.get(i + 1)) == 0);
			if (!twoCardsSameRank) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Compare all main cards and return false if any of them is not the same
	 * kind from others
	 * 
	 * @param mainCards
	 *            An array of mainCards whose length is cardsRequired() of the
	 *            evaluator
	 * @return true if all the cards are same kind and the length of mainCards
	 *         is equal to cardsRequired of the evaluator, false otherwise
	 */
	protected boolean mainCardsSameKind(Card[] mainCards) {
		if (mainCards.length != cardsRequired()) {
			return false;
		} else {
			ArrayList<Card> mainCardsList = new ArrayList<Card>();
			cardsListGen(mainCards, mainCardsList);
			return allSameKind(mainCardsList);
		}
	}

}
