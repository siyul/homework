package lab4;

import java.util.Scanner;

public class NumberPower {
	public static void main (String[] args){
		Scanner scanner = new Scanner(System.in);
		int first = getNextNumber(scanner);
		int second = getNextNumber(scanner);
		
		int result = (int) Math.pow(first, second);
		System.out.println( first + "^" + second + "=" + result);
	}
	//Helper Method
	private static int getNextNumber(Scanner scanner)
	{
		System.out.print("Enter a Number");
		int next;
		if ( scanner.hasNextInt() == false)
		{
			next = 1;
		}
		else
		{
			next = scanner.nextInt();
		}
		return next;
	}
}
