package lab4;

import java.util.Scanner;

public class Bibliography {
	 public static void main(String[] args)
	  {
	    String s1 = "Dijkstra, Edsger#Go To Statement Considered Harmful#Communications of the ACM#1968#11";
	    String s2 = "Levoy, Marc#Display of Surfaces from Volume Data#IEEE Computer Graphics and Applications#1988#8";
	    String s3 = "Dean, Jeffrey; Ghemawat, Sanjay#MapReduce: Simplified Data Processing on Large Clusters#Communications of the ACM#2008#51";
	    BibItem item1 = myHelperMethod(s1);
	    System.out.println(item1);
	    BibItem item2 = myHelperMethod(s2);
	    System.out.println(item2);
	    BibItem item3 = myHelperMethod(s3);
	    System.out.println(item3);
	  }
	 
			 
	 private static BibItem myHelperMethod(String s)
	 {
	   // parse the given string s and return a new BibItem
		Scanner toParse = new Scanner(s);
		toParse.useDelimiter("#");
		String author = toParse.next();
		String title = toParse.next();
		String journal = toParse.next();
		int year = toParse.nextInt();
		int volume = toParse.nextInt();
		BibItem result = new BibItem(author, title, journal, year, volume);
		return result;
	 }
}
