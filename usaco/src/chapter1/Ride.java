package chapter1;

/*
 ID: siyul1
 LANG: JAVA
 TASK: PROG: ride
 */
import java.io.*;
import java.util.*;

class Ride {
	public static void main(String[] args) throws IOException {
		BufferedReader f = new BufferedReader(new FileReader("test.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(
				"test.out")));

		StringTokenizer comet = new StringTokenizer(f.readLine());
		int cIndex = 0;
		ArrayList<Integer> comets = new ArrayList<Integer>();
		double cometMul = 1;
		String curWord = comet.nextToken();
		while (cIndex < curWord.length()) {
			char curChar = curWord.charAt(cIndex);
			comets.add((int) curChar - 64);
			cometMul = cometMul * comets.get(cIndex);
			cIndex++;
		}

		StringTokenizer group = new StringTokenizer(f.readLine());
		int gIndex = 0;
		ArrayList<Integer> groups = new ArrayList<Integer>();
		int groupMul = 1;
		String curWord1 = group.nextToken();
		while (gIndex < curWord1.length()) {
			char curChar1 = curWord1.charAt(gIndex);
			groups.add((int) curChar1 - 64);
			groupMul = groupMul * groups.get(gIndex);
			gIndex++;
		}

		// Calculate the result of multiplication of comet
		if ((int) (groupMul % 47) == (int) (cometMul % 47)) {
			out.println("GO");
		} else {
			out.println("STAY");
		}

		f.close();
		out.close();
		System.exit(0); // don't omit this!
	}
}
