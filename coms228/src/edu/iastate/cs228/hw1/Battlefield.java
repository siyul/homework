package edu.iastate.cs228.hw1;

import java.util.Arrays;

/**
 * A Battlefield can only contain something of type MilitatryTransport or things
 * that inherit from MilitatryTransport
 * 
 * @author Siyu Lin
 * 
 * @param <T>
 *            The type of Battlefield can be MilitaryTransport or something
 *            inherit from MilitaryTransport
 */
public class Battlefield<T extends MilitaryTransport> {

	/**
	 * The array which stores all the MilitaryTransports in the Battlefield
	 */
	private T[] arrayOfMilitaryTransports;

	/**
	 * The number of MilitaryTransports in the Battlefield
	 */
	private int currentSize;

	/**
	 * Constructor for a Battlefield, initially the array of MilitaryTransports
	 * is null
	 * 
	 * @param capacity
	 *            The maximum number of MilitaryTransports in the Battlefield
	 * @throws BattlefieldException
	 *             If capacity is less than or equal to 0, a
	 *             BattlefieldException will be thrown
	 */
	public Battlefield(int capacity) throws BattlefieldException {
		if (capacity <= 0) {
			throw new BattlefieldException("Capacity should be greater than 0");
		} else {
			arrayOfMilitaryTransports = (T[]) new MilitaryTransport[capacity];
			currentSize = 0;
		}
	}

	/**
	 * Get all the MilitaryTransports in the Battlefields
	 * 
	 * @return an array of MilitaryTransports in the Battlefield
	 */
	public MilitaryTransport[] getTransports() {
		return arrayOfMilitaryTransports;
	}

	/**
	 * Get the capacity of the Battlefield
	 * 
	 * @return the length of the array of MilitaryTransports
	 */
	public int getCapacity() {
		return arrayOfMilitaryTransports.length;
	}

	/**
	 * Add a MilitaryTransport to the array of MilitaryTransport's in the first
	 * available cell. If a duplicated name is found, or it will exceed the
	 * capacity, a Battlefield exception will be thrown
	 * 
	 * @param transport
	 *            A MilitaryTransport which can only be the same type of the
	 *            Battlefield Class to be added
	 * @throws BattlefieldException
	 *             A duplicated name is found or the capacity has been reached
	 */
	public void add(T transport) throws BattlefieldException {

		if (nameFound(transport.getName())) {
			throw new BattlefieldException("Name is duplicated");
		}

		else if (isFull()) {
			throw new BattlefieldException(
					"The Battlefield is full, please remove some");
		} else {
			int firstNullIndex;
			for (int i = 0; i < arrayOfMilitaryTransports.length; i++) {
				if (arrayOfMilitaryTransports[i] == null) {
					firstNullIndex = i;
					arrayOfMilitaryTransports[firstNullIndex] = transport;
					currentSize++;
					return;
				}
			}
		}

	}

	/**
	 * This method removes a MilitaryTransport of a given name. When a transport
	 * is moved, the cell previously storing it will become null.
	 * 
	 * @param transportName
	 *            The name of a MilitaryTransport to be removed
	 * @throws BattlefieldException
	 *             If the name cannot be found in the array of the
	 *             MilitaryTransport
	 */
	public void remove(String transportName) throws BattlefieldException {
		if (isEmpty()) {
			throw new BattlefieldException("The Battlefield is empty");
		} else if (!nameFound(transportName)) {
			throw new BattlefieldException("The name does not exist");
		} else {
			int i = 0;
			while (i < arrayOfMilitaryTransports.length) {
				if (arrayOfMilitaryTransports[i] != null
						&& arrayOfMilitaryTransports[i].getName().equals(
								transportName)) {
					arrayOfMilitaryTransports[i] = null;
					// Remove one and then decrease the current size
					currentSize--;
					// We only remove one transport because we assume that all
					// the transports' name are different
					return;
				}
				i++;
			}
		}

	}

	private boolean nameFound(String transportName) {
		if (isEmpty()) {
			return false;
		} else {
			for (int i = 0; i < arrayOfMilitaryTransports.length; i++) {
				if (arrayOfMilitaryTransports[i] != null
						&& transportName.equals(arrayOfMilitaryTransports[i]
								.getName())) {
					return true;
				}
			}
			return false;
		}
	}

	/**
	 * This method simulates one round of interactions amongst
	 * MilitaryTransports. One will interact others from the first cell. When
	 * one MilitaryTransport's defense gets to 0, it will be removed after this
	 * round but can still interact during the round.
	 * 
	 * @throws BattlefieldException
	 *             If there is no MilitaryTransports in the Battlefield
	 */
	public void simulate() throws BattlefieldException {
		if (currentSize == 0) {
			throw new BattlefieldException(
					"There are not enough transports to interact. Please add more transports.");
		} else if (currentSize == 1) {
			// One Transport does not interact
			return;
		} else {
			int index = 0;
			while (index < arrayOfMilitaryTransports.length) {
				for (int i = 0; i < arrayOfMilitaryTransports.length; i++) {

					// If the attacker is not null and the defender is not null
					// and their indices are difference, then we do interact
					if (arrayOfMilitaryTransports[index] != null
							&& arrayOfMilitaryTransports[i] != null
							&& i != index) {
						arrayOfMilitaryTransports[index]
								.interact(arrayOfMilitaryTransports[i]);
					}
				}
				index++;
			}
			for (int i = 0; i < arrayOfMilitaryTransports.length; i++) {
				if (arrayOfMilitaryTransports[i] != null
						&& arrayOfMilitaryTransports[i].getDefense() <= 0) {
					this.remove(arrayOfMilitaryTransports[i].getName());
				}
			}
		}

	}

	/**
	 * Return the array of MilitaryTransport's toString representations in
	 * sorted order. The sorting order is based off of a MilitaryTransport's
	 * defense.
	 * 
	 * @return the array of MilitaryTransport's toString representations in
	 *         sorted order
	 */
	public String[] listTransportsSorted() {
		if (isEmpty()) {
			return null;
		} else {
			String[] allTransportString = new String[currentSize];
			T[] tempArray = (T[]) new MilitaryTransport[currentSize];

			int index = 0;
			while (index < currentSize) {
				for (int i = 0; i < arrayOfMilitaryTransports.length; i++) {
					if (arrayOfMilitaryTransports[i] != null) {
						tempArray[index] = arrayOfMilitaryTransports[i];
						index++;
					}
				}
			}
			Arrays.sort(tempArray);

			for (int i = 0; i < currentSize; i++) {
				allTransportString[i] = tempArray[i].toString();
			}
			return allTransportString;
		}

	}

	/**
	 * Return true if the Battlefield has reached its capacity
	 * 
	 * @return true if the Battlefield has reached its capacity, false otherwise
	 */
	private boolean isFull() {
		for (T e : arrayOfMilitaryTransports) {
			if (e == null) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Return true if the Battlefield is empty
	 * 
	 * @return true if the Battlefield is empty, false otherwise
	 */
	private boolean isEmpty() {
		int i = 0;
		while (i < arrayOfMilitaryTransports.length) {
			if (arrayOfMilitaryTransports[i] != null) {
				return false;
			}
			i++;
		}
		return true;
	}

}
