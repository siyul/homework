package edu.iastate.cs228.hw1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * JUnit Test Class for Battlefield, all tests not required are tagged with @Ignore
 * 
 * @author Siyu Lin
 * 
 */
public class BattlefieldTest {
	Battlefield<MilitaryTransport> mtBattlefield;
	Battlefield<Tank> tkBattlefield;
	Tank tank80;
	Jeep jeep80;
	Tank tank5;
	Jeep jeep20;
	ArmoredVehicle armvh10;

	@Before
	public void setup() throws BattlefieldException {
		mtBattlefield = new Battlefield<MilitaryTransport>(10);
		tkBattlefield = new Battlefield<Tank>(10);
		tank80 = new Tank("testTank80", 80);
		jeep80 = new Jeep("testJeep80", 80);
		tank5 = new Tank("testTank5", 5);
		jeep20 = new Jeep("testJeep20", 20);
		armvh10 = new ArmoredVehicle("testArmV10", 10);
	}

	// Checks if a BattlefieldException is thrown when you create a battlefield
	// with a capacity of 0
	// @Ignore
	@Test(expected = BattlefieldException.class)
	public void testVoidCapacity() throws BattlefieldException {
		Battlefield<MilitaryTransport> b = new Battlefield<MilitaryTransport>(0);
	}

	// @Ignore @Test(expected= BattlefieldException.class)
	// public void testNegativeDefense() throws BattlefieldException{
	// Tank tk = new Tank("tk", -20);
	// }

	// Checks if getCapacity behaves correctly when you create a battlefield
	// with a capacity of 5
	// @Ignore
	@Test
	public void testGetCapacity() throws BattlefieldException {
		Battlefield<MilitaryTransport> b = new Battlefield<MilitaryTransport>(5);
		String msg = "Battlefield<T> constructed with capacity 5 should return 5 when calling getCapacity";
		assertEquals(msg, 5, b.getCapacity());
	}

	// Test if BattlefieldException is thrown when adding transports has caused
	// overflow in Battlefield
	@Ignore
	@Test(expected = BattlefieldException.class)
	public void testBattlefieldOverflow() throws BattlefieldException {
		Battlefield<MilitaryTransport> b = new Battlefield<MilitaryTransport>(2);
		b.add(tank80);
		b.add(jeep80);
		b.add(tank5);
	}

	// Add one tank and Jeep, remove one tank, and then add another tank, test
	// if it added to the first null position
	@Ignore
	@Test
	public void testBattlefieldRemoveAfterOverflow()
			throws BattlefieldException {
		Battlefield<MilitaryTransport> b = new Battlefield<MilitaryTransport>(2);
		b.add(tank80);
		b.add(jeep80);
		b.remove("testTank80");
		b.add(tank5);
		assertEquals(tank5.toString(), b.getTransports()[0].toString());
	}

	// A new Tank is added to a valid ArmoredVehicle battlefield, the first cell
	// will contain the tank
	// @Ignore
	@Test
	public void testTankInAVBattlefield() throws BattlefieldException {
		Battlefield<ArmoredVehicle> b = new Battlefield<ArmoredVehicle>(2);
		b.add(tank80);
		String msg = "Empty Battlefield<ArmoredVehicle> after adding Tank(\"testTank\", 80) will return it in its first cell";
		assertEquals(msg, tank80.toString(), b.getTransports()[0].toString());
	}

	// A new Tank and a new Jeep are added to a valid battlefield of
	// MilitaryTransport, the first cell and the second cell will contain them
	// @Ignore
	@Test
	public void testTankJeepBattlefield() throws BattlefieldException {
		Battlefield<MilitaryTransport> b2 = new Battlefield<MilitaryTransport>(
				2);
		b2.add(tank80);
		b2.add(jeep80);
		String msg = "Empty Battlefield<ArmoredVehicle> after adding Tank(\"testTank\", 80) and Jeep(\"testJeep\", 80) will return them in its first two cells";
		assertEquals(msg, tank80.toString(), b2.getTransports()[0].toString());
		assertEquals(msg, jeep80.toString(), b2.getTransports()[1].toString());
	}

	// Test a BattlefieldException is thrown if we try to remove a nonexistent
	// name
	// @Ignore
	@Test(expected = BattlefieldException.class)
	public void testRemoveName() throws BattlefieldException {
		mtBattlefield.add(tank80);
		mtBattlefield.add(jeep80);
		mtBattlefield.remove("test");
	}

	// Test the battlefield is empty after add two elements and then remove them
	@Ignore
	@Test
	public void testRemoveName2() throws BattlefieldException {
		mtBattlefield.add(tank80);
		mtBattlefield.add(jeep80);
		mtBattlefield.remove("testTank80");
		mtBattlefield.remove("testJeep80");
		assertTrue(isBattlefieldEmpty(mtBattlefield));
	}

	// Check if BattlefieldException is thrown when you add a MilitaryTransport
	// with the same name
	// @Ignore
	@Test(expected = BattlefieldException.class)
	public void testAddDulicatedName() throws BattlefieldException {
		mtBattlefield.add(tank80);
		mtBattlefield.add(jeep80);
		Tank tank2 = new Tank("testTank80", 80);
		mtBattlefield.add(tank2);
	}

	// Test one round of simulation and check their defenses are correct
	// @Ignore
	@Test
	public void testSimulation() throws BattlefieldException {
		mtBattlefield.add(armvh10);
		mtBattlefield.add(tank5);
		mtBattlefield.add(jeep20);
		mtBattlefield.simulate();
		assertEquals(
				"After one round, the defense of ArmoredVehicle(\"testArmV\", 10) is 14",
				14, armvh10.getDefense());
		assertEquals(
				"After one round, the defense of Tank(\"testTank\", 5) is 3",
				3, tank5.getDefense());
		assertEquals(
				"After one round, the defense of Jeep(\"testTank\", 20) is 0",
				0, jeep20.getDefense());
	}

	// Test if the listTransportsSorted can list all the transported in sorted
	// order
	@Ignore
	@Test
	public void testListTransportsSorted() throws BattlefieldException {
		mtBattlefield.add(armvh10);
		mtBattlefield.add(tank5);
		mtBattlefield.add(jeep20);
		String[] allTransports = mtBattlefield.listTransportsSorted();
		assertEquals(tank5.toString(), allTransports[0]);
		assertEquals(armvh10.toString(), allTransports[1]);
		assertEquals(jeep20.toString(), allTransports[2]);
		assertEquals(3, allTransports.length);
	}

	// Test one round of simulation and check they will be in sorted order after
	// calling listTransportsSorted
	@Ignore
	@Test
	public void testSimulationResults() throws BattlefieldException {
		mtBattlefield.add(armvh10);
		mtBattlefield.add(tank5);
		mtBattlefield.add(jeep20);
		mtBattlefield.simulate();
		String[] allTransports = mtBattlefield.listTransportsSorted();
		assertEquals("testTank5,3", allTransports[0]);
		assertEquals("testArmV10,14", allTransports[1]);
		assertEquals(2, allTransports.length);
	}

	@Ignore
	@Test(expected = ClassCastException.class)
	public void testAddJeepToTankBattlefield() throws BattlefieldException {
		ArmoredVehicle amv = new ArmoredVehicle("testAMV", 20);
		tkBattlefield.add((Tank) amv);
	}

	// Test if listTransportsSorted will sort according to defense
	@Ignore
	@Test
	public void testSortion() throws BattlefieldException {
		mtBattlefield.add(new Tank("tktk", 20));
		mtBattlefield.add(new Jeep("Jeep", 30));
		mtBattlefield.add(new ArmoredVehicle("amv203", 0));
		String[] allTransports = mtBattlefield.listTransportsSorted();
		assertEquals("amv203,0", allTransports[0]);
		assertEquals("tktk,20", allTransports[1]);
		assertEquals("Jeep,30", allTransports[2]);
	}

	// Test two tanks' simulation
	@Ignore
	@Test
	public void testTwoTanksSimulation() throws BattlefieldException {
		mtBattlefield.add(tank80);
		mtBattlefield.add(tank5);
		mtBattlefield.simulate();
		assertEquals(tank80.toString(),
				mtBattlefield.getTransports()[0].toString());
		assertEquals(tank5.toString(),
				mtBattlefield.getTransports()[1].toString());
	}

	// Test two ArmoredVehicles' simulation
	@Ignore
	@Test
	public void testTwoArmoredVehicleSimulation() throws BattlefieldException {
		ArmoredVehicle armvh20 = new ArmoredVehicle("testAmv20", 20);
		mtBattlefield.add(armvh10);
		mtBattlefield.add(armvh20);
		mtBattlefield.simulate();
		assertEquals(armvh10.toString(),
				mtBattlefield.getTransports()[0].toString());
		assertEquals(armvh20.toString(),
				mtBattlefield.getTransports()[1].toString());
	}

	// Test one tank and one ArmoredVehicles' simulation
	@Ignore
	@Test
	public void testTankVsAmv() throws BattlefieldException {
		mtBattlefield.add(tank80);
		mtBattlefield.add(armvh10);
		mtBattlefield.simulate();
		assertEquals("testTank80,57",
				mtBattlefield.getTransports()[0].toString());
		assertEquals("testArmV10,4",
				mtBattlefield.getTransports()[1].toString());
	}

	// Test one ArmoredVehicle and one tanks' simulation
	@Ignore
	@Test
	public void testAmvVsTank() throws BattlefieldException {
		mtBattlefield.add(armvh10);
		mtBattlefield.add(tank80);
		mtBattlefield.simulate();
		assertEquals("testArmV10,4",
				mtBattlefield.getTransports()[0].toString());
		assertEquals("testTank80,57",
				mtBattlefield.getTransports()[1].toString());
	}

	// Test one jeep and one ArmoredVehicle's simulation and then add one jeep
	@Ignore
	@Test
	public void testJeepVsAmvAddTank() throws BattlefieldException {
		mtBattlefield.add(jeep20);
		mtBattlefield.add(armvh10);
		mtBattlefield.simulate();
		assertEquals("testArmV10,30",
				mtBattlefield.getTransports()[1].toString());
		assertEquals(null, mtBattlefield.getTransports()[0]);
		mtBattlefield.add(tank80);
		assertEquals(tank80.toString(),
				mtBattlefield.getTransports()[0].toString());
	}

	// Test simulation of two rounds
	@Ignore
	@Test
	public void testTwoRounds() throws BattlefieldException {
		mtBattlefield.add(jeep20);
		mtBattlefield.add(tank5);
		mtBattlefield.add(tank80);
		mtBattlefield.add(armvh10);
		mtBattlefield.simulate();
		mtBattlefield.simulate();
		assertNull(mtBattlefield.getTransports()[0]);
		assertEquals("testTank5,17",
				mtBattlefield.getTransports()[1].toString());
		assertEquals("testTank80,57",
				mtBattlefield.getTransports()[2].toString());
		assertNull(mtBattlefield.getTransports()[3]);
	}

	// Test simulation of two rounds in different order
	@Ignore
	@Test
	public void testTwoRounds2() throws BattlefieldException {
		mtBattlefield.add(jeep20);
		mtBattlefield.add(armvh10);
		mtBattlefield.add(tank5);
		mtBattlefield.add(tank80);
		mtBattlefield.simulate();
		mtBattlefield.simulate();
		assertNull(mtBattlefield.getTransports()[0]);
		assertEquals("testArmV10,1",
				mtBattlefield.getTransports()[1].toString());
		assertEquals("testTank5,1", mtBattlefield.getTransports()[2].toString());
		assertEquals("testTank80,40",
				mtBattlefield.getTransports()[3].toString());
	}

	// Return true if the Battlefield is empty
	private <T extends MilitaryTransport> boolean isBattlefieldEmpty(
			Battlefield<T> b) {
		MilitaryTransport[] arrayOfMilitaryTransports = (T[]) b.getTransports();
		int i = 0;
		while (i < arrayOfMilitaryTransports.length) {
			if (arrayOfMilitaryTransports[i] != null) {
				return false;
			}
			i++;
		}
		return true;
	}

}
