package edu.iastate.cs228.hw3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Comparator;
import java.util.ListIterator;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class CS228DoublyLinkedListTest {
	CS228DoublyLinkedList<String> testList;
	ListIterator<String> testIterator;
	CS228DoublyLinkedList<String>[] testListArray;

	@Before
	public void setup() {
		testList = new CS228DoublyLinkedList<String>();
		testListArray = (CS228DoublyLinkedList<String>[]) new CS228DoublyLinkedList[3];
	}

	@Test
	public void testAdd() {
		testList.add("A");
		assertEquals(testList.indexOf("A"), 0);
	}

	@Test(expected = NullPointerException.class)
	public void testAddNull() {
		addFourElements(testList);
		testList.add(null);
	}

	@Test
	public void testInitialState() {
		assertTrue(testList.isEmpty());
	}

	@Test
	public void testInsertion() {
		testList.add("A");
		testList.add(0, "B");
		assertEquals(testList.get(0), "B");
		assertEquals(testList.get(1), "A");
	}

	@Test
	public void testGetElement() {
		testList.add("A");
		testList.add("B");
		assertEquals(testList.get(1), "B");
	}

	@Test
	public void testRemovePosition() {
		addFourElements(testList);
		testList.remove(2);
		assertEquals(testList.get(2), "D");
		assertEquals(testList.size(), 3);
	}

	@Test
	public void testRemoveAll() {
		addFourElements(testList);
		testList.remove(0);
		testList.remove(0);
		testList.remove(0);
		testList.remove(0);
		assertTrue(testList.isEmpty());
		assertEquals(testList.size(), 0);
	}

	@Test(expected = NullPointerException.class)
	public void testRemoveNull() {
		addFourElements(testList);
		testList.remove(null);
	}

	@Test
	public void testClear() {
		addFourElements(testList);
		testList.clear();
		assertTrue(testList.isEmpty());
	}

	@Test
	public void testClearThenAdd() {
		addFourElements(testList);
		testList.clear();
		testList.add("P");
		assertEquals(testList.get(0), "P");
		assertFalse(testList.isEmpty());
		testIterator = testList.listIterator();
		assertEquals(testIterator.next(), "P");
		assertFalse(testIterator.hasNext());
	}

	@Test
	public void testSet() {
		addFourElements(testList);
		testList.set(0, "E");
		testList.set(1, "F");
		assertEquals(testList.get(0), "E");
		assertEquals(testList.get(1), "F");
		assertEquals(testList.size(), 4);
	}

	@Test(expected = NullPointerException.class)
	public void testSetNull() {
		addFourElements(testList);
		testList.set(0, null);
	}

	@Test
	public void testRemoveObject() {
		addFourElements(testList);
		testList.remove("A");
		assertEquals(testList.size(), 3);
		assertEquals(testList.get(0), "B");
		assertEquals(testList.get(1), "C");
	}

	@Test
	public void testRemoveIndex() {
		addFourElements(testList);
		testList.remove(0);
		assertEquals(testList.size(), 3);
		assertEquals(testList.get(0), "B");
		assertEquals(testList.get(1), "C");
	}

	@Test
	public void testIteratorInitialState() {
		testList.add("A");
		testIterator = testList.listIterator();
		assertEquals(testIterator.nextIndex(), 0);
		assertEquals(testIterator.next(), "A");
		assertEquals(testIterator.nextIndex(), 1);
		assertTrue(testIterator.hasPrevious());
		assertFalse(testIterator.hasNext());
	}

	@Test
	public void testIteratorPosition() {
		addFourElements(testList);
		testIterator = testList.listIterator(2);
		assertEquals(testIterator.nextIndex(), 2);
		assertEquals(testIterator.next(), "C");
		assertEquals(testIterator.nextIndex(), 3);
		assertEquals(testIterator.next(), "D");
		assertFalse(testIterator.hasNext());
	}
	
	@Test
	public void testIteratorAdd() {
		testList.add("A");
		testIterator = testList.listIterator();
		testIterator.add("E");
		assertEquals(testList.get(0), "E");
		assertEquals(testList.get(1), "A");
		assertEquals(testIterator.next(), "A");
	}
	
	@Test
	public void testIteratorPreviousAdd() {
		addFourElements(testList);
		testIterator = testList.listIterator(3);
		assertEquals(testIterator.previous(), "C");
		testIterator.add("E");
		assertEquals(testList.get(2), "E");
		assertEquals(testIterator.next(), "C");
	}
	
	@Test (expected = NullPointerException.class)
	public void testIteratorAddNull() {
		testIterator = testList.listIterator();
		testIterator.add(null);
	}

	@Test
	public void testIteratorSet() {
		testList.add("A");
		testIterator = testList.listIterator();
		testIterator.next();
		testIterator.set("B");
		assertEquals(testList.get(0), "B");
	}
	
	@Test (expected = NullPointerException.class)
	public void testIteratorSetNull() {
		testList.add("A");
		testIterator = testList.listIterator();
		testIterator.next();
		testIterator.set(null);
	}

	@Test
	public void testIteratorRemove() {
		addFourElements(testList);
		testIterator = testList.listIterator();
		testIterator.next();
		testIterator.remove();
		assertEquals(testList.get(0), "B");
		assertEquals(testList.size(), 3);
	}

	@Test
	public void testIteratorRemovePrevious() {
		addFourElements(testList);
		testIterator = testList.listIterator(1);
		assertEquals(testIterator.previous(), "A");
		testIterator.remove();
		assertEquals(testList.get(0), "B");
		assertEquals(testList.get(1), "C");
		assertEquals(testList.get(2), "D");
		assertEquals(testList.size(), 3);
	}

	@Test
	public void testIteratorPrevious() {
		addFourElements(testList);
		testIterator = testList.listIterator(3);
		assertEquals(testIterator.previous(), "C");
	}

	@Test
	public void testListAdd() {
		testListArray[1] = new CS228DoublyLinkedList<String>();
		testListArray[1].add("A");
		testIterator = testListArray[1].listIterator();
		assertEquals(testIterator.next(), "A");
	}

	@Test
	public void testEquals() {
		CS228DoublyLinkedList<String> testList2 = new CS228DoublyLinkedList<String>();
		addFourElements(testList2);
		addFourElements(testList);
		assertTrue(testList2.equals(testList));
	}

	@Test
	public void testEquals2() {
		CS228DoublyLinkedList<String> testList2 = new CS228DoublyLinkedList<String>();
		addFourElements(testList2);
		addFourElements(testList);
		testList2.remove("A");
		assertFalse(testList2.equals(testList));
	}

	@Test
	public void testEquals3() {
		CS228DoublyLinkedList<Integer> testList2 = new CS228DoublyLinkedList<Integer>();
		testList2.add(1);
		testList2.add(2);
		testList2.add(3);
		testList2.add(4);
		addFourElements(testList);
		assertFalse(testList2.equals(testList));
	}
	
	@Test (expected = NullPointerException.class)
	public void testEquals4() {
		addFourElements(testList);
		assertFalse(testList.equals(null));
	}

	@Test  (expected = IllegalArgumentException.class)
	public void testIllegalIndex(){
		testList.add(-1, "A");
		testList.add(1, "A");
		testList.set(1, "A");
		testList.set(0, "A");
		addFourElements(testList);
		testList.remove(-1);
		testList.remove(4);
	}
	
	@Test  (expected = IllegalArgumentException.class)
	public void testIllegalIteratorIndex(){
		testIterator = testList.listIterator(-1);
		testIterator = testList.listIterator(1);
	}
	
	
	private void addFourElements(CS228DoublyLinkedList<String> list) {
		list.add("A");
		list.add("B");
		list.add("C");
		list.add("D");
	}
}
