package lab5;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

//import balloon.Balloon;
//import balloon1.Balloon;
//import balloon2.Balloon;
import balloon3.Balloon;
//import balloon4.Balloon;

public class BalloonTests {
	private Balloon bl;

	
	@Before
	public void setup(){
		bl = new Balloon(10);
	}
	
	//Test the Initial Status
	@Test
	public void testInitial(){
		String msg = "A newly constructed Balloon should not be popped in the constructor,";
		assertEquals(msg, false, bl.isPopped());
	}
	
	@Test
	public void testInitial2(){
		Balloon bl1 = new Balloon(-5);
		String msg = "A newly constructed Balloon should not be popped in the constructor,";
		assertEquals(msg, false, bl1.isPopped());
	}
	
	
	@Test
	public void testInitialRadius(){
		String msg = "A newly constructed Balloon should have the radius given in the constructor,";
		assertEquals(msg, 0, bl.getRadius());
		
	}
	
	@Test
	public void testInitialRadius2(){
		Balloon bl1 = new Balloon(5);
		String msg = "A newly constructed Balloon should have the radius given in the constructor,";
		assertEquals(msg, 0, bl1.getRadius());
		
	}
	
	
	//Test the blow method
	
	//Less than the max
	@Test 
	public void testBlow(){
		bl.blow(3);
		String msg = "After calling blow(3) on a Balloon(10) with maximum radius 5, the radius should be";
		assertEquals(msg, 3, bl.getRadius());
		
	}
	
	@Test 
	public void testBlow2(){
		bl.blow(3);
		String msg = "After calling blow(3) on a Balloon(10) with maximum radius 5, the radius should be";
		assertEquals(msg, false, bl.isPopped());
		
	}
	
	//Beyond the max
	@Test
	public void testBlow3(){
		bl.blow(17);
		String msg = "After calling blow(17), the balloon(10)'s radius should be";
		assertEquals(msg, 0, bl.getRadius());
	}
	@Test
	public void testBlow4(){
		bl.blow(17);
		String msg = "A Balloon(10)'s radius has been increased beyond the maximum radius, it should be popped,";
		assertEquals(msg, true, bl.isPopped());
		
	}
	
	//Equal the max
	@Test
	public void testBlow5(){
		bl.blow(10);
		String msg = "After calling blow(10), the balloon(10)'s radius should be";
		assertEquals(msg, 10, bl.getRadius());
	}
	@Test
	public void testBlow6(){
		bl.blow(10);
		String msg = "A Balloon(10) is not popped after calling blow(10) ";
		assertEquals(msg, false, bl.isPopped());
		
	}
	
	//Negative blow test
	@Test
	public void testNegativeBlow(){
		bl.blow(-10);
		String msg = "A Balloon(10) is not popped after calling blow(-10) ";
		assertEquals(msg, false, bl.isPopped());
		
	}
	@Test
	public void testNegativeBlow1(){
		bl.blow(-10);
		String msg = "After calling blow(-10), a Balloon(10) should have radius  ";
		assertEquals(msg, 0, bl.getRadius());
		
	}
	
	//Test balloon(10) after blowing twice
	@Test
	public void testBlowTwice(){
		bl.blow(17);
		bl.blow(3);
		String msg = "After calling blow(17) and blow(3), it should be popped, ";
		assertEquals(msg, true, bl.isPopped());	
	}
	
	@Test
	public void testBlowTwice2(){
		bl.blow(17);
		bl.blow(3);
		String msg = "After calling blow(17) and blow(3), the radius should be, ";
		assertEquals(msg, 0, bl.getRadius());	
	}
	
	@Test
	public void testBlowTwice3(){
		bl.blow(3);
		bl.blow(10);
		String msg = "After blowing twice the popped ballon, the pop status should be, ";
		assertEquals(msg, true, bl.isPopped());	
	}
	
	@Test
	public void testBlowTwice4(){
		bl.blow(3);
		bl.blow(10);
		String msg = "After blowing twice the popped ballon, the pop status should be, ";
		assertEquals(msg, true, bl.isPopped());	
	}
	
	@Test
	public void testBlowTwice5(){
		bl.blow(6);
		bl.blow(6);
		String msg = "After calling blow(6) and blow(6), it is popped";
		assertEquals(msg, true, bl.isPopped());
		
	}
	
	@Test
	public void testBlowTwice6(){
		bl.blow(6);
		bl.blow(6);
		String msg = "After calling blow(6) and blow(6), the radius should be";
		assertEquals(msg,0, bl.getRadius());
		
	}
	
	//Test the deflate method
	@Test
	public void testDeflate(){
		bl.deflate();
		String msg = "A deflated balloon has radius, ";
		assertEquals(msg,0, bl.getRadius());
		
	}
	
	@Test
	public void testDeflate2(){
		bl.deflate();
		String msg = "A deflated balloon's pop status should be,";
		assertEquals(msg, false, bl.isPopped());
		
	}
	
	//After calling the deflate 
	@Test
	public void testDeflate3(){
		bl.deflate();
		bl.blow(3); 
		String msg = "After Blowing the deflated balloon, the radius should be, ";
		assertEquals(msg, 3, bl.getRadius());	
	}
	
	@Test
	public void testDeflate4(){
		bl.deflate();
		bl.blow(3); 
		String msg = "After Blowing the deflated balloon, it should not be popped, ";
		assertEquals(msg, false, bl.isPopped());	
	}
	
	@Test
	public void testDeflate5(){
		bl.pop();
		bl.deflate(); 
		String msg = "A popped balloon has radius of ";
		assertEquals(msg,0, bl.getRadius());	
	}
	
	@Test
	public void testDeflate6(){
		bl.pop();
		bl.deflate(); 
		String msg = "A popped balloon should not be defalted ";
		assertEquals(msg,true, bl.isPopped());	
	}
	
	//Test the deflate and blow
	@Test
	public void testDeflate7(){
		bl.deflate(); 
		bl.blow(3);
		String msg = "After calling deflate and blow(3), the radius should be ";
		assertEquals(msg,3, bl.getRadius());	
	}
	
	@Test
	public void testDeflate8(){
		bl.deflate(); 
		bl.blow(3);
		String msg = "After calling deflate and blow(3), balloon(10) is not popped ";
		assertEquals(msg,false, bl.isPopped());	
	}
	
	@Test
	public void testDeflate9(){
		 bl.blow(3);
		 bl.deflate();
		String msg = "After calling blow(3) and deflate, the radius should be ";
		assertEquals(msg,0, bl.getRadius());	
	}
	
	@Test
	public void testDeflate10(){
		 bl.blow(3);
		 bl.deflate();
		String msg = "After calling blow(3) and deflate, the pop status should be ";
		assertEquals(msg,false, bl.isPopped());	
	}
	
	@Test
	public void testDeflate11(){
		 bl.blow(11);
		 bl.deflate();
		String msg = "After calling blow(11) and deflate, the radius should be ";
		assertEquals(msg,0, bl.getRadius());	
	}
	
	@Test
	public void testDeflate12(){
		 bl.blow(11);
		 bl.deflate();
		String msg = "After calling blow(11) and deflate, the radius should be ";
		assertEquals(msg,true, bl.isPopped());	
	}
	
	//Test for the pop method
	@Test
	public void testPop(){
		bl.pop();
		String msg = "A popped balloon has radius,";
		assertEquals(msg,0, bl.getRadius());
		
	}
	
	@Test
	public void testPop2(){
		bl.pop();
		String msg = "A popped balloon's status should return ,";
		assertEquals(msg, true, bl.isPopped());
		
	}
	
	@Test
	public void testPop3(){
		bl.pop();
		bl.blow(3);
		String msg = "After blowing the popped ballon, the radius should be, ";
		assertEquals(msg, 0, bl.getRadius());
		
	}
	
	@Test
	public void testPop4(){
		bl.pop();
		bl.blow(3);
		String msg = "After blowing the popped ballon, the pop status should be, ";
		assertEquals(msg, true, bl.isPopped());
		
	}
	
}
